//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "/home/gusiev/.config/mine/dwmstatus/manual/cmus",	1,	0},
	
	{"", "/home/gusiev/.config/mine/dwmstatus/manual/wea",     900,      0},
	
	{"", "/home/gusiev/.config/mine/dwmstatus/manual/mail",     1,      0},
	
	 {"", "/home/gusiev/.config/mine/dwmstatus/manual/wifi",     60,      0},
	 
	 {"💻 ", "free -h | awk '/^Mem/ { print $3\"/\"$2\" \" }' | sed s/i//g",	15,		0},

	{"", "/home/gusiev/.config/mine/dwmstatus/manual/backlight",    1,      0},
	
	{"🔊 ", "/home/gusiev/.config/mine/dwmstatus/manual/vol",     1,      0},
	
	{"🕑 ", "date '+%I:%M %p [%d]'",					1,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "¦";
static unsigned int delimLen = 2;
