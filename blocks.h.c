//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "/home/gusiev/.config/mine/qqq/dwmblocks/manual/cmus",	1,	0},

	{"", "/home/gusiev/.config/mine/qqq/dwmblocks/manual/backlight",	5,	0},

	 {"🔊 ", "/home/gusiev/.config/mine/qqq/dwmblocks/manual/vol",     1,      0},

	{"💻 ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	15,		0},

	{"🕑 ", "date '+%Y.%m.%d  %I:%M %p'",					5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ¦";
static unsigned int delimLen = 3;
